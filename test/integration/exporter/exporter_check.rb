describe http('http://127.0.0.1:9107/metrics') do
  its('status') { should eq 200 }
  its('body') { should match /consul_up 1/ }
  its('body') { should match /consul_health_node_status{check="serfHealth",node="consul_exporter-formula.ci.local",status="passing"} 1/ }
end
