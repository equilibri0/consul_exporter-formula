{% from slspath+"/map.jinja" import consul_exporter with context %}

consul_exporter-remove-service:
  service.dead:
    - name: consul_exporter
  file.absent:
    - name: /etc/systemd/system/consul_exporter.service
    - require:
      - service: consul_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: consul_exporter-remove-service

consul_exporter-remove-symlink:
   file.absent:
    - name: {{ consul_exporter.bin_dir}}/consul_exporter
    - require:
      - consul_exporter-remove-service

consul_exporter-remove-binary:
   file.absent:
    - name: {{ consul_exporter.dist_dir}}
    - require:
      - consul_exporter-remove-symlink

consul_exporter-remove-env:
    file.absent:
      - name: {{ consul_exporter.config_dir }}
      - require:
        - consul_exporter-remove-binary
