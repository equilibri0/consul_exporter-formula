{% from slspath+"/map.jinja" import consul_exporter with context %}

consul_exporter-create-user:
  user.present:
    - name: {{ consul_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

consul_exporter-create-group:
  group.present:
    - name: {{ consul_exporter.service_group }}
    - system: True
    - members:
      - {{ consul_exporter.service_user }}
    - require:
      - user: {{ consul_exporter.service_user }}

consul_exporter-bin-dir:
  file.directory:
   - name: {{ consul_exporter.bin_dir }}
   - makedirs: True

consul_exporter-dist-dir:
 file.directory:
   - name: {{ consul_exporter.dist_dir }}
   - makedirs: True

consul_exporter-install-binary:
 archive.extracted:
   - name: {{ consul_exporter.dist_dir }}/consul_exporter-{{ consul_exporter.version }}
   - source: https://github.com/prometheus/consul_exporter/releases/download/v{{ consul_exporter.version }}/consul_exporter-{{ consul_exporter.version }}.{{ grains['kernel'] | lower }}-{{ consul_exporter.arch }}.tar.gz
   - skip_verify: True
   - options: --strip-components=1
   - user: {{ consul_exporter.service_user }}
   - group: {{ consul_exporter.service_group }}
   - enforce_toplevel: False
   - unless:
     - '[[ -f {{ consul_exporter.dist_dir }}/consul_exporter-{{ consul_exporter.version }}/consul_exporter ]]'
 file.symlink:
   - name: {{ consul_exporter.bin_dir }}/consul_exporter
   - target: {{ consul_exporter.dist_dir }}/consul_exporter-{{ consul_exporter.version }}/consul_exporter
   - mode: 0755
   - unless:
     - '[[ -f {{ consul_exporter.bin_dir }}/consul_exporter ]] && {{ consul_exporter.bin_dir }}/consul_exporter -v | grep {{ consul_exporter.version }}'


#consul_exporter-symlink-binary:
#  file.symlink:
#    - name: {{ consul_exporter.bin_dir }}/consul_exporter
#    - target: {{ consul_exporter.dist_dir }}/consul_exporter-{{ consul_exporter.version }}
#    - mode: 0755
